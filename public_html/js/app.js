/* global require */
/* global document */

require.config({
    baseUrl: 'js',
    paths: {
        jquery: 'vendor/jquery',
        customers: 'config/customers'
                //'jquery.ui': 'ext/jquery-ui.min',
//        'jquery.ui.touch-punch': 'vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min',
//        'jquery': 'vendor/jquery',
//        'bootstrap': 'vendor/bootstrap.min',
//        'modernizr': 'vendor/modernizr-2.8.3-respond-1.4.2.min',
//        'animate-colors': 'vendor/bitstorm/jquery.animate-colors.min',
    }
});

require([
    'jquery',
    'vendor/modernizr',
    'wephoto/core',
    'wephoto/home',
    'wephoto/portfolio',
    'wephoto/about',
    'wephoto/prices',
    'wephoto/contact',
    'wephoto/impressum',
    'wephoto/customer'
], function ($, Modernizr,
        Core, Home, Portfolio, About, Prices, Contact, Impressum, Customer) {
    $(document).ready(function () {
        var Util = {
            isIE: function () {
                if (window.navigator.userAgent.indexOf("MSIE ") > 0 ||
                        !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                    return true;
                }
                return false;
            },
            loadModule: function (currentModule) {
                switch (currentModule) {
                    case 'home':
                        Home.__invoke();
                        break;
                    case 'portfolio':
                        Portfolio.__invoke();
                        break;
                    case 'about':
                        About.__invoke();
                        break;
                    case 'prices':
                        Prices.__invoke();
                        break;
                    case 'contact':
                        Contact.__invoke();
                        break;
                    case 'impressum':
                        Impressum.__invoke();
                        break;
                    case 'customer':
                        Customer.__invoke();
                        break;
                    default:
                        Home.__invoke();
                        break;
                }
                return Util;
            },
            registerOnPopStateEventHandler: function (Core) {
                if (!Util.isIE()) {
                    window.onpopstate = function () {
                        Core.changeLocation().adjuctNavigation();
                        Util.loadModule(Core.__currentModule);
                    };
                } else {
                    window.onhashchange = function () {
                        if ($('#carousel-home')) {
                            $('#carousel-home').remove();
                        }
                        Core.changeLocation().adjuctNavigation();
                        Util.loadModule(Core.__currentModule);
                    };
                }
                return Util;
            }
        };
        Core.__invoke();
        Util.loadModule(Core.__currentModule).registerOnPopStateEventHandler(Core);
    });
});

