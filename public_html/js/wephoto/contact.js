define([
    'wephoto/core'
], function (coreModule) {
    var wephotoContact = {
        /**
         * Main entry point
         */
        __invoke: function () {
            coreModule.loadContent(wephotoContact.__module);
            wephotoContact.registerEventHandlers();
        },
        __module: 'contact',
        /**
         * Registers event handlers
         */
        registerEventHandlers: function () {
        }
    };
    return wephotoContact;
});