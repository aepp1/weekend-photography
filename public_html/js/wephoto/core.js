define([
    'jquery'
], function ($) {
    var WephotoCore = {
        __invoke: function () {
            WephotoCore.changeLocation()
                    .adjuctNavigation()
                    .registerEventHandlers();
        },
        __currentModule: null,
        adjuctNavigation: function () {
            $('ul.nav > li.active').removeClass('active');
            $('ul.nav a[href="#' + WephotoCore.__currentModule + '"]').parent().addClass('active');
            return WephotoCore;
        },
        changeLocation: function () {
            if (window.location.hash === '') {
                WephotoCore.__currentModule = 'home';
            } else {
                WephotoCore.__currentModule = WephotoCore.getLocation(window.location.hash);
            }
            return WephotoCore;
        },
        getLocation: function (hash) {
            return hash.replace('#', '').replace('!', '');
        },
        goHome: function (event) {
            window.location = 'http:///' + window.location.host;
        },
        setCurrentModule: function (event) {
            var location = $(this).attr('href');
            if (location == 'undefined' || location == null || typeof location == undefined) {
                location = document.activeElement.href.split('#')[1];
            }
            WephotoCore.__currentModule = WephotoCore.getLocation(location);
        },
        loadContent: function (location, callback) {
            document.activeElement.blur();
            $('#page-content').load('/pages/' + location + '.html', callback);
        },
        registerEventHandlers: function () {
            $('.nav-link').click(function (event) {
                WephotoCore.setCurrentModule(event);
                WephotoCore.adjuctNavigation(event);
            });
            $('#logo-img').click(WephotoCore.goHome);
            return WephotoCore;
        }
    };
    return WephotoCore;
});