define([
    'wephoto/core'
], function (coreModule) {
    var wephotoPrices = {
        /**
         * Main entry point
         */
        __invoke: function () {
            coreModule.loadContent(wephotoPrices.__module);
            wephotoPrices.registerEventHandlers();
        },
        __module: 'prices',
        /**
         * Registers event handlers
         */
        registerEventHandlers: function () {
        }
    };
    return wephotoPrices;
});