define([
    'wephoto/core'
], function (coreModule) {
    var wephotoAbout = {
        /**
         * Main entry point
         */
        __invoke: function () {
            coreModule.loadContent(wephotoAbout.__module);
            wephotoAbout.registerEventHandlers();
        },
        __module: 'about',
        /**
         * Registers event handlers
         */
        registerEventHandlers: function () {
        }
    };
    return wephotoAbout;
});