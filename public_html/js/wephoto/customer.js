define([
    'jquery',
    'wephoto/core'
], function ($, WephotoCore) {
    var WephotoCustomer = {
        __invoke: function () {
//            if (Lib.docCookies.hasItem('wephoto-customer-email') &&
//                    Lib.docCookies.hasItem('wephoto-customer-password')) {
//                if (WephotoCustomer.credentialsValid(
//                        Lib.docCookies.getItem('wephoto-customer-email'),
//                        Lib.docCookies.getItem('wephoto-customer-password'))) {
//                    WephotoCustomer.loadGallery(Lib.docCookies.getItem('wephoto-customer-email'));
//                    return;
//                }
//            }
//            if ($('#login-dialog').length === 0) {
//                $('#login-dialog-container').load('/pages/login.html', function () {
//                    $('#login-dialog').modal('show');
//                    WephotoCustomer.registerEventHandlers();
//                });
//            } else {
//                $('#login-dialog').modal('show');
//                WephotoCustomer.registerEventHandlers();
//            }
            WephotoCustomer.loadGallery();
        },
        __module: 'customer',
//        __customerEmail: null,
//        credentialsValid: function (email, password) {
//            if (Customers[email] !== 'undefined' && Customers[email] === md5(password)) {
//                console.log('success');
//                return true;
//            } else {
//                console.log('fail');
//                return false;
//            }
//        },
        loadGallery: function () {
            document.activeElement.blur();
            $.ajax({url: 'customer-photo/customer.php',
                data: {action: 'login'},
                type: 'post',
                success: function (response) {
                    var dir = 'customer-photo/' + $.parseJSON(response).email + '/',
                            fileextension = ['.png', '.jpg'],
                            thumbnailsContainer = $('<div/>').attr('id', 'gallery-thumbnails')
                            .append($('<h1/>').addClass('font-cursive text-center wephoto-h1').text('Ihre Photos'));
//                            .append($('<button type="button"/>').addClass('btn btn-default').text('Logout')
//                                    .click(WephotoCustomer.logout));
                    $.ajax({
                        //This will retrieve the contents of the folder if the folder is configured as 'browsable'
                        url: dir,
                        success: function (data) {
                            //Lsit all png file names in the page
                            $(data).find("a:contains(" + fileextension[1] + ")").each(function () {
                                var filename = this.href.replace(window.location.host, "").replace("http:///", "");
                                thumbnailsContainer.append($('<a data-gallery/>').attr({
                                    'href': dir + filename,
                                    'title': filename
                                }).append($("<img src=" + dir + filename + "></img>")));
                            });
                            $('#page-content').html(thumbnailsContainer);
                        }
                    });
                },
                error: function () {

                }
            });
//            var dir = 'customer-photo/' + email + '/';
//            var fileextension = ['.png', '.jpg'];
//            $.ajax({
//                //This will retrieve the contents of the folder if the folder is configured as 'browsable'
//                url: dir,
//                success: function (data) {
//                    //Lsit all png file names in the page
//                    $(data).find("a:contains(" + fileextension + ")").each(function () {
//                        var filename = this.href.replace(window.location.host, "").replace("http:///", "");
//                        $("body").append($("<img src=" + dir + filename + "></img>"));
//                    });
//                }
//            });
//            var dir = 'customer-photo/' + email + '/';
//            var fileextension = ['.png', '.jpg'];
//            $.ajax({
//                //This will retrieve the contents of the folder if the folder is configured as 'browsable'
//                url: dir,
//                success: function (data) {
//                    //Lsit all png file names in the page
//                    $(data).find("a:contains(" + fileextension + ")").each(function () {
//                        var filename = this.href.replace(window.location.host, "").replace("http:///", "");
//                        $("body").append($("<img src=" + dir + filename + "></img>"));
//                    });
//                }
//            });
//            WephotoCore.loadContent(WephotoCustomer.__module);
        },
        logout: function () {
            $.ajax({
                //This will retrieve the contents of the folder if the folder is configured as 'browsable'
                url: 'customer-photo/customer.php',
                data: {action: 'logout'},
                type: 'post',
                success: function (data) {
                    WephotoCore.goHome();
                }
            });
        },
//        login: function (event) {
//            event.preventDefault();
//            var email = $('#email').val(), password = $('#password').val();
//            if (WephotoCustomer.credentialsValid(email, password)) {
//                Lib.docCookies.setItem('wephoto-customer-email', email);
//                Lib.docCookies.setItem('wephoto-customer-password', password);
//                $('#login-dialog').unbind('hide.bs.modal');
//                $('#login-dialog').modal('hide');
//                WephotoCustomer.loadGallery(email);
//            } else {
//
//            }
//        },
        registerEventHandlers: function () {
//            $('#login-submit').click(WephotoCustomer.login);
//            $('#login-cancel').click(WephotoCore.goHome);
//            $('#login-dialog').on('hide.bs.modal', WephotoCore.goHome);
        }
    };
    return WephotoCustomer;
});