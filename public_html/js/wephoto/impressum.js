define([
    'wephoto/core'
], function (coreModule) {
    var wephotoImpressum = {
        /**
         * Main entry point
         */
        __invoke: function () {
            coreModule.loadContent(wephotoImpressum.__module);
            wephotoImpressum.registerEventHandlers();
        },
        __module: 'impressum',
        /**
         * Registers event handlers
         */
        registerEventHandlers: function () {
        }
    };
    return wephotoImpressum;
});