/* global document */

define([
    'jquery',
    'wephoto/core',
    'vendor/blueimp/blueimp-gallery',
    'vendor/blueimp/jquery.blueimp-gallery'
], function ($, WephotoCore) {
    var wephotoPortfolio = {
        /**
         * Main entry point
         */
        __invoke: function () {
            WephotoCore.loadContent(wephotoPortfolio.__module, wephotoPortfolio.initializeGallery);
            wephotoPortfolio.registerEventHandlers();
        },
        __module: 'portfolio',
        initializeGallery: function () {
            var thumbDir = 'img/thumbnails/', galleryDir = 'img/gallery/',
                    fileextension = ['.png', '.jpg'],
                    thumbFileName, galleryFileName;
            $.ajax({
                //This will retrieve the contents of the folder if the folder is configured as 'browsable'
                url: thumbDir,
                success: function (data) {
                    //Lsit all png file names in the page
                    $(data).find('a:contains(' + fileextension[1] + ')').each(function () {
                        thumbFileName = this.href.replace(window.location.host, '').replace('http:///', '');
                        galleryFileName = thumbFileName.replace('thumb', 'gallery');
                        $('#gallery-thumbnails').append($('<a data-gallery/>').attr({
                            'href': galleryDir + galleryFileName,
                            'title': galleryFileName
                        }).append($('<img src=' + thumbDir + thumbFileName + '></img>')));
                    });
                }
            });
//            document.getElementById('gallery-thumbnails').onclick = function (event) {
//                event = event || window.event;
//                var target = event.target || event.srcElement,
//                        link = target.src ? target.parentNode : target,
//                        options = {index: link, event: event},
//                links = this.getElementsByTagName('a');
//                blueimp.Gallery(links, options);
//            };
        },
        /**
         * Registers event handlers
         */
        registerEventHandlers: function () {
        }
    };
    return wephotoPortfolio;
});