define([
    'jquery',
    'wephoto/core',
    'vendor/bootstrap'
], function ($, coreModule) {
    var wephotoHome = {
        __invoke: function () {
            coreModule.loadContent(wephotoHome.__module, wephotoHome.initializeCarousel);
        },
        __module: 'home',
        initializeCarousel: function () {
            var dir = 'img/backgrounds/', fileextension = ['.png', '.jpg'], filename;
            $.ajax({
                //This will retrieve the contents of the folder if the folder is configured as 'browsable'
                url: dir,
                success: function (data) {
                    //Lsit all png file names in the page
                    $(data).find('a:contains(' + fileextension[1] + ')').each(function () {
                        filename = this.href.replace(window.location.host, '').replace('http:///', '');
                        $('.carousel-inner').append($('<div/>').addClass('item')
                                .append($('<div/>').addClass('fill')
                                        .css('background-image', 'url(img/backgrounds/' + filename + ')')));
                    });
                    $('.carousel-inner div').eq(0).addClass('active');
                }
            });
            $('#carousel-home').carousel({
                interval: 3000,
                pause: false
            });
        }
    };
    return wephotoHome;
});