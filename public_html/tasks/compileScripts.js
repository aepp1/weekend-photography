/* global module */

module.exports = function (grunt) {
    grunt.registerTask('compileScripts', 'Uses RequireJS to compile js-files', function (optimize) {
        grunt.config.init({
            requirejs: {
                compile: {
                    options: {
                        baseUrl: 'js',
                        mainConfigFile: 'js/app.js',
                        out: 'dist/js/wephoto.min.js',
                        include: ['vendor/require.js/require', 'app'],
//                        dir: 'dist/js',
//                        modules: [{
//                                name: 'app'
//                            }],
                        optimize: optimize
                    }
                }
            }
        });
        grunt.task.run('requirejs');
    });
};