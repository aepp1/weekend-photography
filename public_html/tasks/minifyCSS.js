/* global module */

module.exports = function (grunt) {
    grunt.registerTask('minifyCss', 'Combine and minify CSS files', function () {
        grunt.config.init({
            cssmin: {
                combine: {
                    files: [
                        {src: ['css/*.css'], dest: 'dist/css/main.css'}
                    ]
//                },
//                minify: {
//                    expand: true,
//                    src: 'dist/assets/css/main.css',
//                    dest: './'
                }
            }
        });
        grunt.task.run('cssmin');
    });
};