/* global module */

module.exports = function (grunt) {
    grunt.registerTask('copyFiles', 'Build release for deployment without styles and scripts', function () {
        grunt.config.init({
            copy: {
                default: {
                    files: [
                        {
                            cwd: 'pages/',
                            src: ['**/*'],
                            dest: 'dist/pages/',
                            expand: true
                        },
                        {
                            cwd: 'fonts/',
                            src: ['**/*'],
                            dest: 'dist/fonts/',
                            expand: true
                        },
                        {
                            cwd: 'img/',
                            src: ['**/*'],
                            dest: 'dist/img/',
                            expand: true
                        },
                        {
                            cwd: 'customer-photo/',
                            dot: true,
                            src: ['**/*'],
                            dest: 'dist/customer-photo/',
                            expand: true
                        },
                        {
                            cwd: './',
                            dot: true,
                            src: [
                                'apple-touch-icon.png',
                                'browserconfig.xml',
                                'favicon.ico',
                                'index.html',
                                'tile-wide.png',
                                'title.png',
                                'login.php',
                                'robots.txt',
                                'sitemap.xml',
                                '!.htaccess',
                                '!.htpasswd'
                            ],
                            dest: 'dist/',
                            expand: true
                        }
                    ]
                }
            },
            clean: {
                default: ['dist/**/*']
            }
        });
        grunt.task.run('clean');
        grunt.task.run('copy');
    });
}; 