module.exports = function(grunt) {
    grunt.registerTask('commitToGitRepo', 'Commit to git-repo', function(force) {
        var version = grunt.file.readJSON('package.json').version,
                readme = grunt.file.readJSON('readme.json'),
                info = readme.releases[0].info,
                message = 'Release version: ' + version + '\n',
                infoKey, i;

        for (infoKey in info) {
            message += '\n***' + infoKey + '***\n';
            if (info[infoKey] instanceof Array) {
                for (i in info[infoKey]) {
                    message += info[infoKey][i] + '\n';
                }
            } else {
                message += info[infoKey] + '\n';
            }
        }

        readme.releases[0].version = version;
        grunt.file.write('readme.json', JSON.stringify(readme, null, 2));
        grunt.config.init({
            gitadd: {
                addFiles: {
                    options: {
                        all: true,
                        verbose: true
                    }
                }
            },
            gitcommit: {
                commitChanges: {
                    options: {
                        message: message,
                        verbose: true,
                        allowEmpty: true
                    }
                }
            },
            gitpush: {
                pushToRemote: {
                    options: {
                        remote: 'origin',
                        verbose: true,
                        force: force
                    }
                }
            }
        });
        grunt.task.run('gitadd');
        grunt.task.run('gitcommit');
        grunt.task.run('gitpush');
    });
};