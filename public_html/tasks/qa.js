module.exports = function(grunt) {
    var directoriesConfig = {
        composer: 'vendor',
        composerBin: 'vendor/bin',
        reports: 'log',
        php: 'app',
        js: 'javascript'
    };
    grunt.initConfig({
        directories: directoriesConfig,
        jshint: {
            options: {
                jshintrc: 'tasks/.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js', '<%= directories.js %>/v24/**/*'
            ]
        },
        jsvalidate: {
            files: [
                'Gruntfile.js'
            ]
        },
        jsonlint: {
            files: [
                '*.json'
            ]
        },
        phpcs: {
            application: {
                dir: ['<%= directories.php %>/src/Vergleich24/**/*.php']
            },
            options: {
                bin: '<%= directories.composerBin %>/phpcs',
                standard: 'PSR1',
//                ignore: 'database',
                extensions: 'php',
                reportFile: '<%= directories.reports %>/phpcs/code_sniffer_report.log'
            }
        },
        phpunit: {
            classes: {
                dir: '<%= directories.php %>/test'
            },
            options: {
                bin: '<%= directories.composerBin %>/phpunit',
                bootstrap: 'bootstrap/autoload.php',
                staticBackup: false,
                colors: true,
                noGlobalsBackup: false
            }
        },
//        phpdocumentor: {
//            dist: {
//                bin: '<%= directories.composerBin %>/phpdoc.php',
//                directory: '<%= directories.php %>',
//                target: '<%= directories.reports %>/phpdocs'
//            }
//        },
        shell: {
            phploc: {
                command: [
                    'mkdir -p <%= directories.reports %>/phploc',
                    'php <%= directories.composerBin %>/phploc --log-xml <%= directories.reports %>/phploc/<%= grunt.template.today("isoDateTime") %>.xml <%= directories.php %>'
                ].join('&&')
            }
        }
    });
    grunt.registerTask('qa', [
        'jsvalidate',
//        'jshint',
        'jsonlint',
        'phpcs',
        'phpunit',
//        'phpdocumentor',
        'shell'
    ]);
};