/* global module */

module.exports = function (grunt) {
    grunt.registerTask('incrementVersion', 'Increment current project version', function (type) {
        grunt.config.init({
            bumpup: {
                options: {
                    dateformat: 'YYYY-MM-DD HH:mm',
                    normalize: false
                },
                file: 'package.json'
            }
        });
        grunt.task.run('bumpup:' + type);
    });
};