/* global module */

module.exports = function (grunt) {
    grunt.registerTask('copyToLocalDist', 'Copy the version for distribution to local server', function () {
        grunt.config.init({
            clean: {
                options: {
                    force: true
                },
                default: {
                    src: 'C:/Server/Apache24/htdocs/wephoto/**/*'
                }
            },
            copy: {
                default: {
                    files: [
                        {
                            cwd: 'dist/',
                            dot: true,
                            src: '**/*',
                            dest: 'C:/Server/Apache24/htdocs/wephoto/',
                            expand: true
                        },
                        {
                            cwd: './',
                            dot: true,
                            src: '.htpasswd',
                            dest: 'C:/Server/Apache24/htdocs/',
                            expand: true
                        }
                    ]
                }
            }
        });
        grunt.task.run('clean');
        grunt.task.run('copy');
    });
};