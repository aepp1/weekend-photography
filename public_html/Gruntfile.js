/* global module */

'use strict';
module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);
    grunt.loadTasks('tasks');

    grunt.registerTask('build', function (type, optimize, commit, force) {
        type = type ? type : 'patch';
        force = force ? true : false;
        optimize = optimize ? 'uglify' : 'none';
        grunt.task.run('copyFiles');
        grunt.task.run('minifyCss');
        grunt.task.run('compileScripts:' + optimize);
        if (commit) {
            grunt.task.run('commitToGitRepo:' + force);
            grunt.task.run('incrementVersion:' + type);
        }
        grunt.task.run('copyToLocalDist');
    });

    grunt.registerTask('test', '', function () {
        grunt.task.run('qa');
    });
};